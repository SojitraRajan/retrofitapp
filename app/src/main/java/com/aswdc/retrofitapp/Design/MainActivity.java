package com.aswdc.retrofitapp.Design;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ListView;
import android.widget.Toast;

import com.aswdc.retrofitapp.Api.BlogerApi;
import com.aswdc.retrofitapp.Model_Purchased.PostAdapter;
import com.aswdc.retrofitapp.Model_Purchased.PostList;
import com.aswdc.retrofitapp.Model_Purchased.PurchasedService;
import com.aswdc.retrofitapp.R;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MainActivity extends AppCompatActivity {

    private ListView lstView;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        lstView = findViewById(R.id.main_lst_view);

        getData();
    }

    private void getData(){
        Call<PostList> postList= BlogerApi.getServise().getPostList();
        postList.enqueue(new Callback<PostList>() {
            @Override
            public void onResponse(Call<PostList> call, Response<PostList> response) {
                List<PurchasedService> pstList = response.body().getData().getPurchasedServices();
                lstView.setAdapter(new PostAdapter(MainActivity.this,pstList));
                Toast.makeText(MainActivity.this,"Success",Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onFailure(Call<PostList> call, Throwable t) {
                Toast.makeText(MainActivity.this,"Error Occured",Toast.LENGTH_SHORT).show();
            }
        });
    }
}