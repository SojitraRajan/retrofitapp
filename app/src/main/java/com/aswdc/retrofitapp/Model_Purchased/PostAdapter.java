package com.aswdc.retrofitapp.Model_Purchased;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.aswdc.retrofitapp.R;

import java.util.List;

public class PostAdapter extends BaseAdapter {
    Activity act;
    List<PurchasedService> arrayPostList;

    public PostAdapter(Activity act, List<PurchasedService> arrayPost) {
        this.act = act;
        this.arrayPostList = arrayPost;
    }

    @Override
    public int getCount() {
        return arrayPostList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return Long.parseLong(arrayPostList.get(i).getName());
    }

    private class ViewHolder{
        TextView tvName;
        TextView tvid;

    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        final ViewHolder viewHolder;
        LayoutInflater inflater = act.getLayoutInflater();

        if (view == null){

            view = inflater.inflate(R.layout.lstpost,null,false);
            viewHolder = new ViewHolder();

            viewHolder.tvName=(TextView) view.findViewById(R.id.list_country_tv_name);
            viewHolder.tvid =(TextView) view.findViewById(R.id.list_country_tv_id);
            /*viewHolder.tvemail =(TextView) view.findViewById(R.id.list_country_tv_email);
            viewHolder.tvaddress =(TextView) view.findViewById(R.id.list_country_tv_address);*/

            view.setTag(viewHolder);
        }
        else {
            viewHolder = (ViewHolder) view.getTag();
        }

        viewHolder.tvName.setText(arrayPostList.get(i).getName());
        viewHolder.tvid.setText(arrayPostList.get(i).getId());
        //viewHolder.tvemail.setText(arrayPostList.get(i).getTypename()+"");
        return view;
    }
}
