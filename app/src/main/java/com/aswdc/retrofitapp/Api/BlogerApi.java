package com.aswdc.retrofitapp.Api;

import com.aswdc.retrofitapp.Model_Purchased.PostList;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;

public class BlogerApi {

    public static final String BASE_URL = "https://api.jsonbin.io/";

    public  static   PostServise postServise=null;

    public  static PostServise getServise(){
        if (postServise==null){

              Retrofit   retrofit = new Retrofit.Builder()
                        .baseUrl(BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();

            postServise=retrofit.create(PostServise.class);
        }
        return  postServise;
    }
    public  interface  PostServise{

         @GET("b/5efdf1000bab551d2b6ab1c9/1")
        Call<PostList> getPostList();

    }

}
